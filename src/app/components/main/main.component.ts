import { Component } from '@angular/core';
import { take } from 'rxjs';
import { Character } from 'src/app/models/character.model';
import { CharactersService } from 'src/app/services/characters.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent {
  characters: Character[] = [];
  filteredCharacters: Character[] = [];

  constructor(private charactersService: CharactersService) {
    this.charactersService.data.pipe(take(1)).subscribe((characters) => {
      this.filteredCharacters = characters;
      this.characters = characters;
    });
  }

  filterCharacters(event: any): void {
    this.filteredCharacters = this.characters.filter((charater) => {
      return charater.name
        .toLowerCase()
        .includes(event.target.value.toLowerCase());
    });
  }
}
